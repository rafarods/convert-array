/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eldar;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Given an array of string arguments, join all elements of the array with
 * spaces, convert all letters to lowercase and send the result to STDOUT.
 *
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
public class ConvertArray {

    /**
     * Maximum words accepted
     */
    public static final short MAX_ITEMS = 10;

    /**
     * Array with words
     */
    private final String[] args;

    /**
     * Main class
     *
     * @param args Array with words
     */
    public static void main(String[] args) {
        ConvertArray ca = new ConvertArray(args);
        System.out.println(ca.toLowerCase());
    }

    /**
     * Default constructor
     *
     * @param args Array with words
     */
    public ConvertArray(String[] args) {
        this.args = args;
    }

    /**
     * Convert to lower case with delimiter, the rules are: 1) Only accept ASCII
     * letter and white space 2) Remove all duplicated spaces 3) Remove all non
     * ASCII letters
     *
     * @param delimiter Text delimiter
     * @return Joined words
     */
    public String toLowerCase(String delimiter) {
        return Arrays.asList(this.args)
                .stream()
                .limit(MAX_ITEMS)
                .map((item) -> {
                    return item.replaceAll("[^a-zA-Z\\s+]", "")
                               .replaceAll("^ +| +$|( )+", "$1");
                })
                .map(String::toLowerCase)
                .collect(Collectors.joining(delimiter));
    }

    /**
     * Convert to lower case with space delimiter, the rules are: 1) Only accept
     * ASCII letter and white space 2) Remove all duplicated spaces 3) Remove
     * all non ASCII letters
     *
     * @return Joined words
     */
    public String toLowerCase() {
        return this.toLowerCase(" ");
    }
}
