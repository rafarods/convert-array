/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eldar;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Test class JUnit
 *
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
public class ConvertArrayTest {

    /**
     * List of arguments to convert
     */
    private static final String[][] TEST_ARGS = new String[][]{
        {"FirstWord", "SecondWord", "THIRDWORD"},
        {"FirstWord", "SecondWord", "THIRDWORD", "HelLo    word!"},
        {"FirstWord", "SecondWord", "THIRDWORD", "Fourword", "Fiveword", "SIXword", "SeVeNwOrLd", "EigHtWord", "NineWORD", "TenWord", "NoCount11", "NoCount12"},
        {"*´s´¨klsdäsdlÑñ[:{.{.{.{\"#\"#$#%$$#%$&%$&%&&%/&/'\\"}
    };

    /**
     * List of expected arguments
     */
    private static final String[] TEST_RESULT = new String[]{
        "firstword secondword thirdword",
        "firstword secondword thirdword hello word",
        "firstword secondword thirdword fourword fiveword sixword sevenworld eightword nineword tenword",
        "sklsdsdl"
    };

    /**
     * Test converter method
     */
    @Test
    public void testConvertions() {
        for (int i = 0; i < TEST_ARGS.length; ++i) {
            final ConvertArray conv = new ConvertArray(TEST_ARGS[i]);
            Assertions.assertEquals(TEST_RESULT[i], conv.toLowerCase());
        }
    }
}
